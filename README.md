# Calculator - Android App #

I built a calculator app for the Mobile Computing course at Brock University. Since this was our first assignment (and introduction to Android Studio), we had to follow very specific guidelines, including the calculator's layout and behavior.

My Java code can be viewed in CalculatorMainActivity.java and my design/styling choices in activity_calculator_main.xml.

![calculator](screenshot/calcTransition.png?raw=true)