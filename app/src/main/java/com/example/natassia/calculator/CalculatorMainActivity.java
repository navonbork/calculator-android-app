/*
Natassia Bork
4802955
COSC 3P97
Assignment 1
 */
package com.example.natassia.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class CalculatorMainActivity extends AppCompatActivity {

    double num1 = 0, num2 = 0;      // store first two numbers for applying operator
    boolean operator = false;       // does an operation need to be performed during input

    int op = 0;             // determine the operation needed to perform:
                            // 1 = addition, 2 = subtraction, 3 = multiplication, 4 = division

    TextView tv;            // textview for displaying calculation

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator_main);
        tv = (TextView) findViewById(R.id.textView);   // get textview in activity
    }

    // number pressed by user
    public void numberPressed(int num) {
        // store first number before operator and display
        if (!operator) {
            num1 = num1 * 10 + num; // appending sequence of numbers inputted by user
            tv.setText(Double.toString(num1));
        }
        // store second number after operator and display
        else {
            num2 = num2 * 10 + num;
            tv.setText(Double.toString(num1) + " " + getOpChar(op) + " " + num2);
        }
    }

    // get operator character to display
    private char getOpChar(int op) {
        if (op == 1) return '+';
        if (op == 2) return '-';
        if (op == 3) return '*';
        if (op == 4) return '/';
        return ' ';
    }

    // perform operation
    public void computeNumber() {
        if(op == 1){                            // add
            num1 = num1 + num2;
            tv.setText(String.valueOf(num1));   // replace with computed result
            num2 = 0;
        }
        else if (op == 2) {                     // sub
            num1 = num1 - num2;
            tv.setText(String.valueOf(num1));
            num2 = 0;
        }
        else if (op == 3) {                     // mul
            num1 = num1 * num2;
            tv.setText(String.valueOf(num1));
            num2 = 0;
        }
        else {                                  // div
            if(num2 != 0) {
                num1 = num1 / num2;
                tv.setText(String.valueOf(num1));
                num2 = 0;
            }
            else{                                // division of zero
                tv.setText("Invalid");
                num1 = 0;
                num2 = 0;
                operator = false;
            }
        }
    }

    // operator pressed by user
    public void operatorPressed(int op){
        // perform operation before inputting next operator
        if(operator) {
            computeNumber();
        }
        tv.append(" " + getOpChar(op));
        this.op = op;   // remember pressed operator
        operator = true;
    }

    // perform action for each button click
    public void buttonClick(View v){
        switch(v.getId()) {
            case R.id.button0:
                numberPressed(0);
                break;
            case R.id.button1:
                numberPressed(1);
                break;
            case R.id.button2:
                numberPressed(2);
                break;
            case R.id.button3:
                numberPressed(3);
                break;
            case R.id.button4:
                numberPressed(4);
                break;
            case R.id.button5:
                numberPressed(5);
                break;
            case R.id.button6:
                numberPressed(6);
                break;
            case R.id.button7:
                numberPressed(7);
                break;
            case R.id.button8:
                numberPressed(8);
                break;
            case R.id.button9:
                numberPressed(9);
                break;
            case R.id.buttonAdd:
                operatorPressed(1);
                break;
            case R.id.buttonSub:
                operatorPressed(2);
                break;
            case R.id.buttonMul:
                operatorPressed(3);
                break;
            case R.id.buttonDiv:
                operatorPressed(4);
                break;
            case R.id.buttonClear:
                num1 = 0;
                num2 = 0;
                operator = false;
                tv.setText("0");
                break;
            case R.id.buttonEqual:
                computeNumber();
                operator = false;
                num2 = 0;
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calculator_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
